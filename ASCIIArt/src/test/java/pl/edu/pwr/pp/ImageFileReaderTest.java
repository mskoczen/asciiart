package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ImageFileReaderTest {
	
	ImageFileReader imageReader;
	
	@Before
	public void setUp() {
		imageReader = new ImageFileReader();
	}
	
	@Test
	public void shouldThrowExceptionWhenFileDontExist() {
		// given
		String fileName = "nonexistent.pgm";
		try {
			// when
			imageReader.readPgmFile(fileName);
			// then
			
		} catch (Exception e) {
			Assert.fail("Should throw exception");
		}
		
	}
}
