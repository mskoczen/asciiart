package pl.edu.pwr.pp;import static org.junit.Assert.*;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetScaledDimensionTest {
	
	private Dimension dim;
	private Dimension expected;

	@Before
	public void setUp() throws Exception {
		dim = new Dimension(1920, 1080);
		expected = new Dimension(3840, 2160);
	}

	@Test
	public void ShouldBe4KFromWidth() {
		Dimension out = MainFrame.getScaledDimension(dim, new Dimension(3840, 999999999));
		assertTrue(out.getHeight() == expected.getHeight());
	}
	@Test
	public void ShouldBe4KFromHeight() {
		Dimension out = MainFrame.getScaledDimension(dim, new Dimension(999999999, 2160));
		assertTrue(out.getWidth() == expected.getWidth());
		
	}
	@Test 
	public void ShouldThrowNull()
	{
		Boolean bool = false;

		BufferedImage i = MainFrame.resize(new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_GRAY), 1920, 1080);
		
		Assert.assertTrue(i.getHeight() == 1080);
	}

}
