package pl.edu.pwr.pp;


import static org.junit.Assert.*;
import pl.edu.pwr.pp.MainFrame;
import org.junit.Test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetFileExtensionTest {

	@Test
	public void ShouldReturnPGM() {
		String fileName = "nonexistent.pgm";
		Assert.assertEquals("pgm", MainFrame.getFileExtension(fileName));
	}
	
	@Test
	public void ShouldReturnEmptyString() {
		String fileName = "dsfsdfjsjidfkhsdfkhsdkhfs";
		Assert.assertEquals("", MainFrame.getFileExtension(fileName));
	}

}
