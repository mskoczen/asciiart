package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.stream.IntStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;


public class MainFrame extends JFrame {

	BufferedImage loadedImage;
	private String currentFile = "";
	private JPanel contentPane;
	JComboBox lblOption1;
	JComboBox lblOption2;
    private JLabel image = new JLabel("");
    private JButton btnSaveToFile = new JButton("Zapisz do pliku");
    private JPanel panelImage = new JPanel();
    private String[] opt1={"Niska","Wysoka"};
    private String[] opt2={"80 znak�w","160 znak�w","500 znak�w","Szeroko�� ekranu","Bez skalowania"};

    /**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 438, 339);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		panelImage.setBorder(BorderFactory.createLineBorder(Color.black));
		panelImage.setBackground(Color.YELLOW);
		contentPane.add(panelImage, BorderLayout.CENTER);
		panelImage.setLayout(new FlowLayout());
	
		panelImage.add(image);
		
		
		
		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new GridLayout(6, 1));
		panelButtons.setBorder(BorderFactory.createLineBorder(Color.black));
		panelButtons.setBackground(Color.GREEN);
		contentPane.add(panelButtons, BorderLayout.WEST);
		
		
		lblOption1 = new JComboBox(opt1);
		lblOption1.setBorder(BorderFactory.createLineBorder(Color.black));
		
		lblOption2 = new JComboBox(opt2);
		lblOption2.setBorder(BorderFactory.createLineBorder(Color.black));
		
		
		JButton btnReadImage = new JButton("Wczytaj obraz");
		btnReadImage.setHorizontalAlignment(SwingConstants.CENTER);
		btnReadImage.addActionListener(new ActionListener ()
        {
			public void actionPerformed(ActionEvent ae) {
				final DialogReadImage dialogWindow = new DialogReadImage();
	        	dialogWindow.setVisible(true);
	        	dialogWindow.okButton.addActionListener(new ActionListener ()
			    {
	        		public void actionPerformed(ActionEvent ae) {
	        			if (dialogWindow.fromDisc.isSelected()&& dialogWindow.lblFromFile.getText()!="�cie�ka do pliku:") {
	        				dialogWindow.setVisible(false);
	        				try {
	        					btnSaveToFile.setEnabled(true);
	        					ReadImageFromDisk(dialogWindow.lblFromFile.getText());
	        				} catch (URISyntaxException e) {
	        					// TODO Auto-generated catch block
	        					e.printStackTrace();
	        				}
				        	dialogWindow.dispose();
	        			}
				        if (dialogWindow.fromURL.isSelected()&&dialogWindow.txtURL.getText()!="") {
				        	String extension = MainFrame.getFileExtension(dialogWindow.txtURL.getText());
					        if (extension != null) {
					            if (extension.equals("tiff") ||
					                extension.equals("tiff") ||
					                extension.equals("gif") ||
					                extension.equals("jpeg") ||
					                extension.equals("jpg") ||
					                extension.equals("png") ||
					                extension.equals("pgm")) {
					            	
					            	dialogWindow.setVisible(false);
						        	btnSaveToFile.setEnabled(true);
						        	ReadImageFromUrl(dialogWindow.txtURL.getText());
						        	dialogWindow.dispose();
					            }
					            else
					            {
					            	dialogWindow.lblReadFile.setText("Niepoprawny URL");
					            }
					        }
				        	
				        }
	        		}
			    });
			}
        });
	                 

		btnSaveToFile.setHorizontalAlignment(SwingConstants.CENTER);
		btnSaveToFile.setEnabled(false);
		btnSaveToFile.addActionListener(x -> {

			
			try {
				int[][] intensities;
				
				if(loadedImage.getType()!= BufferedImage.TYPE_BYTE_GRAY)
				{
					BufferedImage imageToSave;
					switch (lblOption2.getSelectedIndex())
					{
						case 0:
							imageToSave = resize(loadedImage, 80, 10000);
							break;
						case 1:
							imageToSave = resize(loadedImage, 160, 10000);
							break;
						case 2:
							imageToSave = resize(loadedImage, 500, 10000);
							break;
						case 3:
							Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
							imageToSave = resize(loadedImage, screenSize.width, 100000);
							//imageToSave = resize(loadedImage, panelImage.getWidth(), 100000);
							break;
						default:
							imageToSave = loadedImage;
					}
					ImageConverter con = new ImageConverter();
					intensities= con.colorToGrey(imageToSave);
				}
				else
					intensities = new ImageFileReader().readPgmFile(currentFile);
				char[][] ascii;
				if(lblOption1.getSelectedItem()==opt1[0])
					ascii = ImageConverter.intensitiesToAscii(intensities,false);
				else
					ascii = ImageConverter.intensitiesToAscii(intensities,true);
				JFileChooser saveFile = new JFileChooser();
				int returnVal = saveFile.showSaveDialog(MainFrame.this);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					new ImageFileWriter().saveToTxtFile(ascii, saveFile.getSelectedFile().getPath()+".txt");
				}
				
			} catch (URISyntaxException er) {
				er.printStackTrace();
			} 
			
		});

		JButton btnFunction1 = new JButton("[Funkcja1]");
		btnFunction1.setHorizontalAlignment(SwingConstants.CENTER);

		JButton btnFunction2 = new JButton("[Funkcja2]");
		btnFunction2.setHorizontalAlignment(SwingConstants.CENTER);
		
		panelButtons.add(btnReadImage);
		panelButtons.add(lblOption1);
		panelButtons.add(lblOption2);
		panelButtons.add(btnSaveToFile);
		panelButtons.add(btnFunction1);
		panelButtons.add(btnFunction2);
		
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				
				if (loadedImage != null)
				{
					image.setIcon(new ImageIcon(resize(loadedImage, panelImage.getWidth(), panelImage.getHeight())));
				}
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	private BufferedImage GetPGMImage(String path) throws URISyntaxException
	{
		int[][] pgmData = new ImageFileReader().readPgmFile(path);
		BufferedImage image = new BufferedImage(pgmData[0].length,pgmData.length ,
			     BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = image.getRaster();
		IntStream.range(0, pgmData.length).forEach(
				y -> {for (int x = 0; (x < pgmData[0].length); x++) {
				    raster.setSample(x, y, 0, pgmData[y][x]);
				}});
		
		image.setData(raster);
		return image;
		
	}
	void ReadImageFromDisk(String path) throws URISyntaxException
	{
		try {
			if (getFileExtension(path).equals("pgm"))
			{
				loadedImage = GetPGMImage(path);
				//btnSaveToFile.setEnabled(true);
			}
			else
				loadedImage = ImageIO.read(new File(path));
			currentFile = path;
			image.setIcon(new ImageIcon(resize(loadedImage, panelImage.getWidth(), panelImage.getHeight())));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	void ReadImageFromUrl(String URL)
	{
		try {
			URL url = new URL(URL);
			loadedImage = ImageIO.read(url);
			image.setIcon(new ImageIcon(resize(loadedImage, panelImage.getWidth(), panelImage.getHeight())));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	static String getFileExtension(String path) {
        if(path.lastIndexOf(".") != -1 && path.lastIndexOf(".") != 0)
        return path.substring(path.lastIndexOf(".")+1);
        else return "";
    }
	static BufferedImage resize(BufferedImage img, int newW, int newH) {  
		Dimension old = new Dimension(img.getWidth(), img.getHeight());
		Dimension panelSize = new Dimension(newW, newH);
		//Dimension newD = new Dimension(newW, newH);
		Dimension newD = getScaledDimension(old, panelSize);
	    int w = img.getWidth();  
	    int h = img.getHeight();
	    
	    BufferedImage dimg = new BufferedImage(newD.width, newD.height, img.getType());  
	    Graphics2D g = dimg.createGraphics();  
	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	    RenderingHints.VALUE_INTERPOLATION_BILINEAR);  
	    g.drawImage(img, 0, 0, newD.width, newD.height, 0, 0, w, h, null);  
	    g.dispose();  
	    return dimg;  
	}
	static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {

	    int original_width = imgSize.width;
	    int original_height = imgSize.height;
	    int bound_width = boundary.width;
	    int bound_height = boundary.height;
	    int new_width = original_width;
	    int new_height = original_height;

	    
	    //scale width to fit
	    new_width = bound_width;
	    //scale height to maintain aspect ratio
	    new_height = (new_width * original_height) / original_width;
	    

	    // then check if we need to scale even with the new height
	    if (new_height > bound_height) {
	        //scale height to fit instead
	        new_height = bound_height;
	        //scale width to maintain aspect ratio
	        new_width = (new_height * original_width) / original_height;
	    }
	    

	    return new Dimension(new_width, new_height);
	}
}
