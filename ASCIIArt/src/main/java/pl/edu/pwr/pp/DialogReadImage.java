package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class DialogReadImage extends JDialog {

	
	private final JPanel contentPanel = new JPanel();
	public JRadioButton fromDisc = new JRadioButton("Z dysku");
	public JLabel lblFromFile = new JLabel("�cie�ka do pliku:");
	public JRadioButton fromURL = new JRadioButton("Z adresu URL");
	public JFormattedTextField txtURL = new JFormattedTextField("");
	public JButton okButton = new JButton("OK");
	public ButtonGroup group = new ButtonGroup();
	public JLabel lblReadFile = new JLabel("Wczytaj plik:");
	
 	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DialogReadImage dialog = new DialogReadImage();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	@SuppressWarnings("static-access")
	public DialogReadImage() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		contentPanel.add(lblReadFile, BorderLayout.NORTH);
		
		JPanel readImageOptions = new JPanel();
		contentPanel.add(readImageOptions,BorderLayout.CENTER);
		readImageOptions.setLayout(new GridLayout(4,1));
		
		JPanel fromDiskOptions=new JPanel();
		fromDiskOptions.setLayout(new FlowLayout());
		
		fromDisc.setSelected(true);
		group.add(fromDisc);
		group.add(fromURL);
		
		JButton btnFromDisc = new JButton("Wybierz plik");
		fromDiskOptions.add(fromDisc);
		fromDiskOptions.add(btnFromDisc);
		btnFromDisc.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser openFile = new JFileChooser();
				openFile.addChoosableFileFilter(new ImageFilter());
				openFile.setAcceptAllFileFilterUsed(false);
				
				int returnVal = openFile.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					lblFromFile.setText(openFile.getSelectedFile().getPath());
				}
			}
		} );
		
		fromURL.setHorizontalAlignment(SwingConstants.LEFT);
		

		readImageOptions.add(fromDiskOptions);
		readImageOptions.add(lblFromFile);
		readImageOptions.add(fromURL);
		readImageOptions.add(txtURL);
		
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener ()
		        {
			           public void actionPerformed(ActionEvent ae) {
			        	   dispose();
			           }
				});
			}
		}
	}
}
