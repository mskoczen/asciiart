package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class ImageConverter {

	private int parameterRed=2989;
	private int parameterBlue=5870;
	private int parameterGreen=1140;
			
	public int[][] colorToGrey(BufferedImage image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		int intensities[][]=new int[height][];
		for(int i =0;i<height;++i)
			intensities[i]= new int[width];
		
		int y;
		for(int row = 0; row<height;++row)
		{
			for(int column=0; column<width;++column)
			{
				Color color=new Color(image.getRGB(column, row));
				y=(color.getBlue()*parameterBlue+color.getRed()*parameterRed+color.getGreen()*parameterGreen)/10000;
				intensities[row][column]=y;
			}
		}
		return intensities;
	}
	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";
	public static String INTENSITY_2_ASCIIHIGH = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. "; 
	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity, Boolean quality) {
		// TODO Wasz kod
		String INTENSITY_ASCII;
		if(quality)
			INTENSITY_ASCII=INTENSITY_2_ASCIIHIGH;
		else
			INTENSITY_ASCII=INTENSITY_2_ASCII;
		Double tableLength = (double) INTENSITY_ASCII.length();
		Double intervalWidth = 256/tableLength;
		Double temp;
		int lowerLimit, upperLimit =-1;
		int i;
		for(i=0; i<INTENSITY_ASCII.length();i++)
		{
			lowerLimit=upperLimit;
			temp=(i+1)*intervalWidth;
			upperLimit=temp.intValue();
			if(intensity>lowerLimit&& intensity<=upperLimit)
			{
				break;
			}
		}
		return INTENSITY_ASCII.charAt(i);
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities, Boolean quality) {
		// TODO Wasz kod
		char [][] asciiTable = new char[intensities.length][intensities[0].length];
		for(int i =0; i<intensities.length;++i)
		{
			for(int j =0;j<intensities[i].length;++j)
				asciiTable[i][j]=intensityToAscii(intensities[i][j],quality);
		}
		return asciiTable;
	}

}
